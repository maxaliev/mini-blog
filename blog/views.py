from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, reverse
from django.views.generic import DetailView, ListView, TemplateView
from django.utils import timezone
from django.contrib.auth.views import login_required
from django.utils.decorators import method_decorator

from .models import BlogPost, Comment, User


@method_decorator(login_required, name='dispatch')
class HomePageView(TemplateView):
    template_name = 'blog/index.html'


class BlogListView(ListView):
    model = BlogPost
    context_object_name = 'ordered_posts'
    paginate_by = 5
    template_name = 'blog/blogs.html'

    def get_queryset(self):
        return BlogPost.objects.filter(pub_date__lte=timezone.now())


class BlogDetailView(DetailView):
    model = BlogPost
    template_name = 'blog/detail.html'


class CreateCommentView(DetailView):
    model = BlogPost
    template_name = 'blog/create_comment.html'


def add_comment(rq, post_id):
    try:
        post = get_object_or_404(BlogPost, pk=post_id)
    except (post.DoesNotExist, KeyError):
        return render(rq, 'blog/create_comment.html', {
            'post_id': post_id,
            'error_message': 'Post is not exist'
        })
    else:
        comment = Comment.objects.create(blog_id=post_id,
                                         author=rq.user.profile,
                                         comment_text=rq.POST['comment-text'],
                                         pub_date=timezone.now())
        comment.save()
        return HttpResponseRedirect(reverse('blog:detail', args=(post_id,)))


def validate_new_user(rq):
    if User.objects.filter(username=rq.POST['username']).first():
        return render(rq, 'blog/../templates/registration/registration.html', {
            'error_message': 'Username has already been taken.'
        })
    elif len(rq.POST['password']) < 6:
        return render(rq, 'blog/../templates/registration/registration.html', {
            'error_message': 'Password must contain at least 6 symbols.'
        })
    elif User.objects.filter(email=rq.POST['email']).first():
        return render(rq, 'blog/../templates/registration/registration.html', {
            'error_message': 'Email has already been registered.'
        })
    else:
        user = User.objects.create_user(
            rq.POST['username'],
            rq.POST['email'],
            rq.POST['password'],
        )

        user.save()
        return HttpResponseRedirect(reverse('blog:index'))


class AuthorListView(ListView):
    model = User
    template_name = 'blog/authors.html'


class AuthorDetailView(TemplateView):
    template_name = 'blog/author.html'

