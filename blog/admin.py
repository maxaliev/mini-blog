from django.contrib import admin

from .models import BlogPost, Comment, Profile


class InlineBlogPost(admin.TabularInline):
    model = BlogPost


class InlineComment(admin.TabularInline):
    model = Comment


class ProfileAdmin(admin.ModelAdmin):
    inlines = [InlineBlogPost]


class BlogPostAdmin(admin.ModelAdmin):
    inlines = [InlineComment]


admin.site.register(Profile, ProfileAdmin)
admin.site.register(BlogPost, BlogPostAdmin)
