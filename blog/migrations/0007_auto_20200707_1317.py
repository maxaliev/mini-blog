# Generated by Django 3.0.7 on 2020-07-07 10:17

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20200707_1314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpost',
            name='pub_date',
            field=models.DateTimeField(verbose_name=datetime.datetime(2020, 7, 7, 10, 17, 34, 752472, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='pub_date',
            field=models.DateTimeField(verbose_name=datetime.datetime(2020, 7, 7, 10, 17, 34, 752472, tzinfo=utc)),
        ),
    ]
