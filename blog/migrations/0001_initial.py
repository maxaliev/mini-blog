# Generated by Django 3.0.7 on 2020-06-29 10:19

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('author', models.CharField(max_length=50)),
                ('pub_date', models.DateTimeField(verbose_name=datetime.datetime(2020, 6, 29, 10, 19, 15, 863208, tzinfo=utc))),
                ('content', models.TextField()),
            ],
            options={
                'ordering': ['-pub_date'],
            },
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=50)),
                ('pub_date', models.DateTimeField(verbose_name=datetime.datetime(2020, 6, 29, 10, 19, 15, 863208, tzinfo=utc))),
                ('comment_text', models.CharField(max_length=300)),
                ('blog', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='blog.BlogPost')),
            ],
            options={
                'ordering': ['-pub_date'],
            },
        ),
    ]
