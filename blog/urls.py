from django.urls import path, include
from django.views.generic import TemplateView

from . import views


app_name = 'blog'

urlpatterns = [
    path('home/', views.HomePageView.as_view(), name='index'),
    path('', views.HomePageView.as_view(), name='index'),
    path('account/register/', TemplateView.as_view(template_name='registration/registration.html'), name='register'),
    path('account/register/validate/', views.validate_new_user, name='validate'),
    path('blogs/', views.BlogListView.as_view(), name='blogs'),
    path('blog/<int:pk>/', views.BlogDetailView.as_view(), name='detail'),
    path('authors/', views.AuthorListView.as_view(), name='authors'),
    path('author/<int:pk>', views.AuthorDetailView.as_view(), name='author'),
    path('<int:pk>/create_comment/', views.CreateCommentView.as_view(), name='create_comment'),
    path('<int:post_id>/add_comment/', views.add_comment, name='add_comment'),
]

urlpatterns += [
    path('account/', include('django.contrib.auth.urls'))
]