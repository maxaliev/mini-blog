from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(blank=True)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class BlogPost(models.Model):
    class Meta:
        ordering = ['pub_date']

    def __str__(self):
        return self.name

    name = models.CharField(max_length=50)
    author = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    pub_date = models.DateTimeField(timezone.now())
    content = models.TextField()


class Comment(models.Model):
    class Meta:
        ordering = ['-pub_date']

    def __str__(self):
        return self.comment_text

    blog = models.ForeignKey(BlogPost, on_delete=models.CASCADE)
    author = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    pub_date = models.DateTimeField(timezone.now())
    comment_text = models.CharField(max_length=300)

